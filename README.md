# eptssh

使用expect工具, 批量验证ssh公钥/密码的正确性, 批量拷贝公钥对目标主机的脚本工具.

`check_passwd.sh`会尝试直接连接列表中的主机, 如果其所在服务器持有目标主机的公钥权限, 自然可以直接登录, 如果没有, 就会尝试输入列表中的密码, 以验证密码正确性.

`copy_key.sh`是在**当前主机拥有所有目标主机的权限**的情况下, 拷贝另一个公钥到目标主机, 重复公钥不会拷贝.

## 1. 准备

需要一台安装有`except`工具的linux主机.

```
$ yum install except
```

## 2. check_passwd使用方法

`check_passwd.sh`需要与`expect_ssh.sh`放在相同目录下. 其余所需参数需要在`check_passwd.sh`文件中定义...没有用`getopt`, 嫌太麻烦, 总共也就3,4个参数.

1. ip_list: 待检测IP/密码列表文件, 该文件的格式为: `192.168.1.1 passwd`(以空格分隔).

2. USER: ssh登录用户, 全局只能设置一个, 所有`ip_list`文件中的主机都将使用这个用户尝试登录. 当然, 要二次定义的话也很容易的, 只需要修改`check_passwd.sh`与`ip_list`表示的文件格式即可, 因为是调用`except.sh`脚本.

3. login_log: 输出日志, 不过比较杂乱, 需要使用sublime/vscode等文本编辑器二次处理.

最后, 注意保证`check_passwd.sh`与`expect_ssh.sh`拥有执行权限.

直接执行`./check_passwd.sh`即可.

## 3. copy_key使用方法

`copy_key.sh`需要与`except_ssh.sh`, `expect_ssh_copy_id.sh`放在相同目录下, 需要有可执行权限. 参数在`copy_key.sh`中定义.

`ip_list`: IP/密码对应列表文件名

`USER`: ssh登录用户

`key_file`: 待导入公钥名称

`import_log`: 导入结果日志. 内容也很杂乱, 需要手动筛选.

其他日志什么的不重要.

------

## FAQ

1. 注意: 所有文件的换行符必须为Unix类型, 否则可能会出现许多匪夷所思的问题, 尽量提前避免吧.